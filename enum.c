#include <stdio.h>
#include <stdint.h>

typedef enum {
    JANUARY = 1,
    FEBRUARY = 1,
    MARCH = 1,
    APRIL = 2,
    MAY,
    JUNE = 100,
    JULY,
    AUGUST,
    SEPTEMBER = 1,
    OCTOBER,
    NOVEMBER,
    DECEMBER
} MONTHS;

typedef union {
    uint8_t arr[4];
    uint32_t full;
} IP;

typedef union {
    int* ptr;
    int array[1];
} Other;



int main() {
    // printf("JANUARY - %d\n", JANUARY);
    // printf("FEBRUARY - %d\n", FEBRUARY);
    // printf("MARCH - %d\n", MARCH);
    // printf("APRIL - %d\n", APRIL);
    // printf("MAY - %d\n", MAY);
    // printf("JUNE - %d\n", JUNE);
    // printf("JULY - %d\n", JULY);
    // printf("AUGUST - %d\n", AUGUST);
    // printf("SEPTEMBER - %d\n", SEPTEMBER);
    // printf("OCTOBER - %d\n", OCTOBER);
    // printf("NOVEMBER - %d\n", NOVEMBER);
    // printf("DECEMBER - %d\n", DECEMBER);
    
    
    IP ip = {.arr = {192, 168, 0, 100}};
    printf("%d\n", ip.full);
    
    for( int i = 0; i < 4; i++ ) {
        printf("%hhu\n", ip.arr[i]);
    }
    
    printf("%hhu.%hhu.%hhu.%hhu\n", ip.arr[0], ip.arr[1], ip.arr[2], ip.arr[3]);
    return 0;
}



