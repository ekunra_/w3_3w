#include <stdio.h>

typedef int (*F)(int, int);


int sum(int x, int y) {
    return x + y;
}

int sub(int x, int y) {
    return x - y;
}

int mult(int x, int y) {
    return x * y;
}

int div(int x, int y) {
    return x / y;
}

F fabric(int action) {
    F arr[] = {sum, sub, mult, div};
    return arr[action - 1];
}


int main() {
    
    int action;
    int x, y;
    
    printf("Hello!\n");
    printf("------------------------------\n");
    while (1) {
        printf("Please enter an action you wanna do:\n");
        printf(" 1. Add.\n");
        printf(" 2. Subtract.\n");
        printf(" 3. Multiply.\n");
        printf(" 4. Divide.\n");
        printf(" 5. Exit.\n");
        
        printf("\nYour choice: ");
        scanf("%d", &action);
        
        if ( (action > 5) || (action < 1) ) {
            printf("You entered a wrong number. Try again!!!\n");
            printf("------------------------------\n");
            continue;
        }
        
        if ( action == 5 ) {
            printf("See you soon!!\n");
            printf("------------------------------\n");
            break;
        }
        
        printf("\nEnter two numbers: ");
        scanf("%d %d", &x, &y);
        
        printf("\nRESULT: %d\n", fabric(action)(x, y));
        printf("------------------------------\n");
    }
    
    
    return 0;
}
