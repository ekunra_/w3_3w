#include <iostream>

int sum(int x, int y = 100) {
    std::cout << "INT ";
    return x + y;
}

double sum(double x, double y) {
    std::cout << "DOUBLE ";
    return x + y;
}

float sum(float x, float y) {
    std::cout << "FLOAT ";
    return x + y;
}

int flightToTheMoon(int mass, int sunwing = 3, int something = 60, int speed = 1, int power = 2) {
    return mass * speed / power + sunwing + something;
}


int main() {
    int x;
    float y = 3.25;
    
    // std::cout << sum(2) << std::endl;
    // std::cout << sum(2.2, 5.1) << std::endl;
    // std::cout << sum(y, y) << std::endl;
    
    std::cout << flightToTheMoon(20) << std::endl;
    std::cout << flightToTheMoon(70, 2) << std::endl;
    std::cout << flightToTheMoon(70, 4) << std::endl;
    std::cout << flightToTheMoon(70, 4, 2) << std::endl;
    std::cout << flightToTheMoon(70, 4, 3, 3, 70) << std::endl;
    std::cout << flightToTheMoon(70, something=70) << std::endl;
    
    return 0;
}
