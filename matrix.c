#include <stdio.h>

#define SIZE 3


// void matrixRotate90(int target[SIZE][SIZE], int source[SIZE][SIZE], int size);
// Поворот матрицы на 90 градусов по часовой стрелке.

void matrixRotate90(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    for( int row = 0; row < size; row++ ) {
        for ( int col = 0, cot = size - 1; col < size; col++, cot-- ) {
            target[row][col] = source[cot][row];
        }
    }
}


void matrixPrint(int matrix[SIZE][SIZE], int size) {
    for( int row = 0; row < SIZE; row++ ) {
        int last = size - 1;
        for( int col = 0; col < last; col++ ) {
            printf("%d ", matrix[row][col]);
        }
        printf("%d\n", matrix[row][last]);
    }
}


int main() {
    char c = 'c';
    
    int array1[10];
    int array2[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    
    int matrix1[3][3];
    int matrix2[SIZE][SIZE];
    int matrix3[][3] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int target[SIZE][SIZE];
    
    matrixPrint(matrix3, SIZE);
    matrixRotate90(target, matrix3, SIZE);
    printf("--------------------\n");
    matrixPrint(target, SIZE);
    
    return 0;
}
