#include <stdio.h>
#include <stdlib.h>

void print(int array[], int size) {
    int limit = size - 1;
    
    printf("[");
    for( int i = 0; i < limit; i++ ) {
        printf("%d, ", array[i]);
    }
    if (limit <= 0) {
        printf("]\n");
    } else {
        printf("%d]\n", array[limit]);
    }
}

void fillArray(int array[], int size, int qty) {
    int counter = 1;
    for( int i = 0; i < qty && i < size; i++) {
        array[i] = counter;
        counter += 1;
    }
}

int main() {
    int x;
    int* ptr = (int*)malloc(sizeof(int));
    
    printf("x   address    : %p\n", &x);
    printf("ptr address    : %p\n", &ptr);
    printf("ptr address(in): %p\n", ptr);
    
    
    
    int arraySize = 50;
    
    int arr1[arraySize];
    int* arr2 = (int*)malloc(arraySize * sizeof(int));
    
    print(arr2, arraySize);
    printf("----------------\n");
    fillArray(arr2, arraySize, 35);
    print(arr2, arraySize);
    printf("----------------\n");
    
    int newSize = arraySize + arraySize;
    int *arr2 = (int*)realloc(arr2, sizeof(int) * newSize); //NULL
    
    if (temp) {
        arr2 = temp;
        arraySize = newSize;
    } else {
        // FREE SOME SPACE!!!
    }
    
    print(arr2, arraySize);
    
    free(ptr);
    free(arr2);
    ptr = NULL;
    arr2 = NULL;
    
    return 0;
}
